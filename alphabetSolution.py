from itertools import product

def main():
    print("Please enter the file location of the crossword you would like to search: ")
    input_file = input()
    fin = open(input_file, "r")

    # Retrieve array dimensions
    size_info = fin.readline() 

    # Get row and col from first line of file
    middle = size_info.find('x')
    end = size_info.find('\n')
    rows = int(size_info[0: middle])
    cols = int(size_info[middle + 1: end])

    # Create list of letters in crossword
    letters = []
    count = 0
    for line in fin:
        letters  += [line.strip().replace(" ", "")]

        # Loop breaks before answer key
        if count >= rows - 1:
            break
        count  += 1

    # Populate the answer key
    key_words = []
    for line in fin:
        key_words  += [line.strip().replace(" ", "")]

    # Iterate through the crossword
    for i, j, k in product(range(rows), range(cols), range(len(key_words))):
        if letters [i][j] == key_words[k][0]:
            get_direction(i, j, k, 1, rows, cols, key_words, letters)

# Find a direction to search based on first and second letter in word
def get_direction(i_start, j_start, key_word_index, letter_num, rows, cols, key_words, letters):
    for n, m in product(range(-1, 2), range(-1, 2)):
        # Skip negative and neutral indexing
        if (i_start + n < 0) or (j_start + m < 0): continue
        if n == m == 0: continue

        try:
            next_letter = letters[i_start + n][j_start + m]
            # Search directionally if second letter of keyWord is found in crossword
            if next_letter == key_words[key_word_index][letter_num]: 
                search_direction(i_start, j_start, n, m, i_start + n + n, j_start + m + m, \
                                 key_word_index, letter_num + 1, rows, cols, key_words, letters)
        # Skip out of bounds indexing
        except IndexError: 
            continue  

# Check if the crossword letters in a direction match the letters in the keyword
def search_direction(i_start, j_start, row_dir, col_dir, row, col, key_word_index, letter_num, rows, cols, key_words, letters):
    if (row < 0) or (col < 0): return

    try:
        word_length = len(key_words[key_word_index]) - 1
        key_word_letter = key_words[key_word_index][letter_num]

        # Keep searching if the letters match
        if letters[row][col] == key_word_letter:
            if letter_num == word_length:
                # Terminate upon complete match
                print(f"{key_words[key_word_index]} {i_start}:{j_start} {row}:{col}")
            else:
                search_direction(i_start, j_start, row_dir, col_dir, row + row_dir, col + col_dir, \
                                 key_word_index, letter_num + 1, rows, cols, key_words, letters)
    except IndexError: 
        return

if __name__ == "__main__":
    main()
